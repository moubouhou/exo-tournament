package com.nespresso.sofa.recruitement.tournament.fighter;

public abstract class Fighter {

	private int hitPoints;

	private int damage;

	private Buckler buckler;
	
	private Armor armor;

	private boolean hasAxe;

	public Fighter(int hitPoints, int damage, Buckler buckler, boolean hasAxe) {

		this.hitPoints = hitPoints;
		this.damage = damage;
		this.buckler = buckler;
		this.hasAxe = hasAxe;

	}
	
	public Fighter(int hitPoints, int damage, Armor armor, boolean hasAxe) {

		this.hitPoints = hitPoints;
		this.damage = damage;
		this.armor = armor;
		this.hasAxe = hasAxe;

	}

	public Fighter(int hitPoints, int damage) {

		this.hitPoints = hitPoints;
		this.damage = damage;

	}

	public void engage(Fighter anotherFighter) {

		boolean isDead = false;
		while (!isDead) {
			this.blow(anotherFighter);
			anotherFighter.blow(this);
			isDead = hitPoints() <= 0;
		}

	}

	public int hitPoints() {
		return this.hitPoints;
	}

	public void setHitPoints(int newHitPoints) {
		if (newHitPoints < 0)
			this.hitPoints = 0;
		else
			this.hitPoints = newHitPoints;
	}

	public int damage() {
		return this.damage;
	}

	public Buckler getBuckler() {
		return this.buckler;
	}

	public boolean hasAxe() {
		return hasAxe;
	}

	public void blow(Fighter anotherFighter) {
		boolean blockedBlow = anotherFighter.blockBlow(this);
		if (!blockedBlow) {
			anotherFighter.setHitPoints(anotherFighter.hitPoints()
					- this.damage());
		}

	}
	
	private boolean blockBlow(Fighter anotherFighter) {
		if (this.getBuckler() != null) 
			return this.getBuckler().blockBlow(anotherFighter.hasAxe());
		return false;
		
	}
	
	public void setDamage(int damage) {
		this.damage = damage;
	}

	public void setBuckler(Buckler buckler) {
		this.buckler = buckler;
	}

	public void setHasAxe(boolean hasAxe) {
		this.hasAxe = hasAxe;
	}

}
