package com.nespresso.sofa.recruitement.tournament.fighter;

public class Highlander extends Fighter {

	public static final int INTIAL_HIGHLANDER_HITPOINTS = 150;
	public static final int INTIAL_HIGHLANDER_DAMAGE = 12;

	public Highlander() {

		super(INTIAL_HIGHLANDER_HITPOINTS, INTIAL_HIGHLANDER_DAMAGE);

	}

	public Highlander(Buckler b) {
		super(INTIAL_HIGHLANDER_HITPOINTS, INTIAL_HIGHLANDER_DAMAGE, b, false);
	}

	public Highlander equip(String typeEquip) {
		switch (typeEquip) {
		case "buckler":
			Buckler buckler = new Buckler();
			return createBucklerHighlander(buckler);

		default:
			return new Highlander();
		}
	}

	private Highlander createBucklerHighlander(Buckler buckler) {
		return new Highlander(buckler);
	}

}
