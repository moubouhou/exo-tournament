package com.nespresso.sofa.recruitement.tournament.fighter;

public class Buckler {

	private static final int INITIAL_NB_BLOCKING_BLOWS = 3;

	private int blockingBlows;

	private boolean hasBlockedBlow = true;

	public Buckler() {
		this.blockingBlows = INITIAL_NB_BLOCKING_BLOWS;
	}

	public boolean blockBlow(boolean withAxe) {
		boolean canBlockBlows = canBlockBlows();
		if (!canBlockBlows) {
			return false;
		}
		if (hasBlockedBlow) {
			if (withAxe)
				blockAxeBlow();
		}
		boolean lastHasBlockedBlow = hasBlockedBlow;
		hasBlockedBlow = !hasBlockedBlow;
		return lastHasBlockedBlow;
	}

	private boolean canBlockBlows() {
		return (blockingBlows > 0);
	}

	private void blockAxeBlow() {
		if (blockingBlows > 0)
			blockingBlows--;

	}

	public boolean hasBlockedBlow() {
		return hasBlockedBlow;
	}

}
