package com.nespresso.sofa.recruitement.tournament.fighter;

public class Viking extends Fighter {

	public static final int INTIAL_VIKING_HITPOINTS = 120;
	public static final int INTIAL_VIKING_DAMAGE = 6;

	public Viking() {
		super(INTIAL_VIKING_HITPOINTS, INTIAL_VIKING_DAMAGE);
	}

	public Viking(Buckler b) {
		super(INTIAL_VIKING_HITPOINTS, INTIAL_VIKING_DAMAGE, b, true);
	}

	public Viking equip(String typeEquip) {
		switch (typeEquip) {
		case "buckler":
			Buckler buckler = new Buckler();
			return createBucklerViking(buckler);

		default:
			return new Viking();
		}
	}

	private Viking createBucklerViking(Buckler buckler) {
		return new Viking(buckler);
	}
}
