package com.nespresso.sofa.recruitement.tournament.fighter;

public class Swordsman extends Fighter {

	public static final int INTIAL_SWORDSMAN_HITPOINTS = 100;
	public static final int INTIAL_SWORDSMAN_DAMAGE = 5;

	public Swordsman() {

		super(INTIAL_SWORDSMAN_HITPOINTS, INTIAL_SWORDSMAN_DAMAGE);

	}

	public Swordsman(Buckler b) {
		super(INTIAL_SWORDSMAN_HITPOINTS, INTIAL_SWORDSMAN_DAMAGE, b, false);
	}
	
	public Swordsman(Armor a) {
		super(INTIAL_SWORDSMAN_HITPOINTS, INTIAL_SWORDSMAN_DAMAGE, a, false);
	}

	public Swordsman equip(String typeEquip) {
		switch (typeEquip) {
		case "buckler":
			Buckler buckler = new Buckler();
			return createBucklerSwordsman(this, buckler);
		case "armor":
			Armor armor = new Armor();
			return createArmorSwordsman(this, armor);
		default:
			return new Swordsman();
		}
	}

	private Swordsman createArmorSwordsman(Swordsman originSwordsman, Armor armor) {
		Swordsman swordsman =  new Swordsman(armor);
		swordsman.setHitPoints(originSwordsman.hitPoints());
		swordsman.setDamage(originSwordsman.damage());
		swordsman.setHasAxe(originSwordsman.hasAxe());
		return swordsman;
	}

	private Swordsman createBucklerSwordsman(Swordsman originSwordsman, Buckler buckler) {
		Swordsman swordsman =  new Swordsman(buckler);
		swordsman.setHitPoints(originSwordsman.hitPoints());
		swordsman.setDamage(originSwordsman.damage());
		swordsman.setHasAxe(originSwordsman.hasAxe());
		return swordsman;
	}

}
